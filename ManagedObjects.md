# Les objets managés

## Définition

Les objets managés sont les objets manipulés par l'ORM. Afin de pouvoir être convenablement gérés il est nécessaire de :

 * Dériver de la classe parent `DBObject` directement ou indirectement
 * Créer le mapping correspondant à au type de cet objet
 

## Création d'un nouveau type d'objet managé

Pour créer un nouveau type d'objet managé, il faut à la fois qu'il soit mappé correctement (voir mapping) et qu'il dérive de la classe de base `DBObject` ou de toute autre classe dérivant directement ou indirectement de `DBObject`.

## Instanciation et insertion en base de données

L'instanciation d'un objet managé a lieu dans un contexte. Comme le contexte est intégré dans la couche abstraite d'accès aux données, la création d'un objet managé se fait depuis un objet de type `DBAL` - DataBase Abstract Layer.

Exemple :

On créé un objet `Bank` dans le contexte de la DBAL `dbal`.

```obj-c
DBAL *dbal = [DBAL defaultDBAL];
Bank *bank1 = [dbal newObject:@"Bank"];
bank1.name = @"big bank";
```
L'instanciation ne provoque pas l'insertion de l'objet en base. Pour qu'il le soit il faut le demander explicitement via la méthode `insert`, ou bien qu'il soit associé à un objet / un tableau lié à la base de données en base de données (ce qui signifie soit déjà inséré, soit récupéré de la BDD en amont).

Exemple 1 : 

```obj-c
[bank1 insert];
```

Ici on insère directement l'objet `bank1`.

Exemple 2 :

```obj-c
Client *c1 = [dbal newObject:@"Client"];
c1.firstname = @"bobix";
c1.lastname = @"chap";
c1.age = @15;
c1.bank = bank1;

[c1 insert];
```

Ici l'objet bank1 est inséré car il a été associé à l'objet c1 qui a été ensuite inséré.


## Manipulation

La manipulation d'un objet managé s'effectue de la même manière qu'un objet classique. Si l'objet a été préalablement inséré, toute récupération/modification d'une propriété ou d'une relation aura un impact équivalent en base de données, car cet objet est lié à la BDD.


```obj-c
Client *c1 = [dbal newObject:@"Client"];
[c1 insert];

c1.firstname = @"bobix";
c1.lastname = @"chap";
c1.age = @15;
c1.bank = bank1;
```

## Récupération depuis la base

**TODO**

Lorsqu'un objet est récupéré depuis la DBAL (bdd pour eMaket) il est de ce fait lié à la base de données. Toutes modification d'une ou plusieurs de ses relations ou propriétés sera donc repercuté sur la base de données. 

### Extension pour eMaket
 
Les objets managés sont étendus pour l'application eMaket du laboratoire. Ils dérivent tous de la classe `eObjet` qui elle même dérive de `DBObject`. Il s'agit d'une simple surcouche permettant de proposer de nombreuses méthodes spécifiques aux besoins de eMaket.

TODO (voir méthodes spécifique ?)

# Les tableaux managés

## Definition

Les tableaux managés sont les tableaux manipulés par l'ORM. Afin de pouvoir être convenablement gérés il est nécessaire de :

 * Dériver de la classe parent `DBMutableArray` directement ou indirectement
 
## Instanciation et insertion en base de données

TODO
 
## Récupération d'un tableau managé depuis la base de données

TODO

## Liaison avec la base de données

TODO

## Ajouter un objet dans le tableau

TODO

## Supprimer un objet dans le tableau

TODO

## Remplacer un objet dans le tableau

TODO


 
### Extension pour eMaket
 
Les tableaux managés sont étendus pour l'application eMaket du laboratoire. Ils sont nommés `nMutableArray` et sont équivalent aux `DBMutableArray`. Il s'agit d'une simple surcouche permettant de proposer de nombreuses méthodes spécifiques aux besoins de eMaket.
 
 
