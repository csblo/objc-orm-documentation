    
# Utilisation de l'ORM dans eMaket

- [Introduction](#introduction)
- [Où est déclarée bdd ?](#où-est-déclarée-bdd-)
- [Initialisation de l'objet de manipulation de base de données](#initialisation-de-lobjet-de-manipulation-de-base-de-données)
- [Gestion des bases de données](#gestion-des-bases-de-données)
    - [Créer une base de données](#créer-une-base-de-données)
    - [Générer les tables de la base de données depuis le mapping](#générer-les-tables-de-la-base-de-données-depuis-le-mapping)
    - [Sélectionner une base de données](#sélectionner-une-base-de-données)
    - [Vérifier l'existence d'une base de données](#vérifier-lexistence-dune-base-de-données)
    - [Lister les bases de données](#lister-les-bases-de-données)
    - [Vider le contenu d'une base de données](#vider-le-contenu-dune-base-de-données)
    - [Supprimer une base de données](#supprimer-une-base-de-données)

- [Gestion des fichiers](#gestion-des-fichiers)
    - [Copier un fichier local vers le serveur distant](#copier-un-fichier-local-vers-le-serveur-distant)
    - [Supprimer un fichier distant](#supprimer-un-fichier-distant)
    - [Remplacer un fichier distant par un autre](#remplacer-un-fichier-distant-par-un-autre)
    - [Créer un répertoire sur le serveur distant](#créer-un-répertoire-sur-le-serveur-distant)
    - [Supprimer un répertoire sur le serveur distant](#supprimer-un-répertoire-sur-le-serveur-distant)
    - [Renommer un répertoiresur le serveur distant](#renommer-un-répertoire-sur-le-serveur-distant)
    - [Savoir si le chemin distant correspond à un fichier ou un répertoire](#savoir-si-le-chemin-distant-correspond-à-un-fichier-ou-un-répertoire)
    - [Savoir si le fichier ou répertoire distant existe](#savoir-si-le-fichier-ou-répertoire-distant-existe)
    - [Récupérer un fichier distant en local](#récupérer-un-fichier-distant-en-local)
    - [Générer un nom de fichier unique](#générer-un-nom-de-fichier-unique)

- [Exemple de manipulation de la base de données](#exemples-de-manipulation-de-la-base-de-données)
    - [Création d'un objet](#création-dun-objet)
    - [Insertion d'un objet](#insertion-dun-objet)
    - [Modification d'un objet](#modification-dun-objet)


## Introduction

L'ORM posséde une surcouche pour `eMaket` définie dans le framework `eObjet`. Ainsi certains noms de méthodes ou de propriétés permettant la gestion de la base ou des objets peuvent différer. Par exemple, il peuvent être en français au lieu d'être en anglais. De plus eMaket possède une couche abstraite d'accès aux données (DBAL) déjà déclarée auparavant sous la forme d'une variable nommé `bdd`.

Cette documentation a pour but de montrer les étapes nécessaires pour faire fonctionner l'ORM dans `eMaket` avec le framework `eObjet`.

## Où est déclarée bdd ?

L'objet `bdd` qui permet la manipulation de la base de données, création, lecture, modification et suppression d'objets se trouve dans la classe `BDD`. Cet objet est accessible depuis n'importe quelle autre classe car il est déclaré de façon global.

## Initialisation de l'objet de manipulation de base de données

Pour commencer à utiliser l'ORM il  faut ajouter les lignes de code suivantes dans `AppDelegate.m` de l'application :

```obj-c
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //Nom de la base de données avec laquelle on souhaite travailler
    nomBD = @"emaket_ben_2";
    
    //Génération du mapping à partir des annotations de classes
    [Mapping generateXML:@"mapping.xml"];
    
    //Création de la couche d'accès aux données
    bdd = [BDD new];
    
    //On place l'ORM en mode connecté
    bdd.estConnecteBDD = YES;
    
    //Les répertoires permettrons d'enregistrer les fichiers sur le serveur distant ou de les récupèrer en local
    bdd.repDepart = nil;
    bdd.repArrivee = nil;
    
    //
    if (bdd.estConnecteBDD)
    {
        //Vérifie si la connexion au serveur est effectuée correctement
        if (![bdd connexionBD_OK])
        {
            [General afficherMessage:@"eMaKet : PROBLEME DE CONNEXION A LA BDD" etInfo:@"Contactez les personnes"];
            return;
        }
        
        //Si base n'existe pas pour nomBD, alors on la créé
        if (![bdd existeDB:nomBD])
          [bdd creerDB:nomBD];
         
        //On selectionne la base de données de nom nomBD sur le serveur
        [bdd selectionDB:nomBD];
        
        //On génére la base de données à partir du mapping XML (généré auparavant) !
        [bdd genererDB];
    }
}
```

Et lorsque l'application se termine :

```obj-c
- (void)applicationWillTerminate:(NSNotification *)aNotification
{
    // On ferme la connexion    
    [bdd fermerConnexionBD];
}
```

## Gestion des bases de données

Il est possible d'effectuer différentes actions sur la base de données, la créer, la générer, la supprimer, etc.

### Créer une base de données

Permet de créer une base de données - un schéma - vide, c'est à dire sans table, de nom `nomBD`.

```obj-c
[bdd creerDB:nomBD];
```

### Générer les tables de la base de données depuis le mapping

Génére la base de données à partir des annotations décrites dans le mapping XML. Les tables et les liens entre les tables seront créés en fonction des entités de leurs champs et de leurs relations décrites. Pour effectuer cette opération, il faut que la base de données existe préalablement

```obj-c
[bdd genererDB];
```

### Sélectionner une base de données

Sélectionne la base de données de nom `nomBD` comme base de données courante. Tous les enregistrements et récupérations d'objets s'effectuent sur la base sélectionnée.

```obj-c
[bdd selectionDB:nomBD];
```

### Vérifier l'existence d'une base de données

```obj-c
BOOL exist = [bdd existeDB:nomBD];
```

### Lister les bases de données

```obj-c
NSArray* listBD = [bdd listeDB];
```

### Vider le contenu d'une base de données

Permet de vider le contenu d'une base de données, c'est à dire ses enregistrements, sans la supprimer.

```obj-c
[bdd viderDB];
```    

### Supprimer une base de données

Supprime totalement une base de données et son contenu sur le serveur.

```obj-c
[bdd supprimeDB:nomBD];
```

## Gestion des fichiers

Les fichiers sont aussi enregistrés sur un dépôt distant - le même serveur que celui hébergeant la base de données. Afin de pouvoir enregistrer et récupérer correctement les fichiers quelques déclarations sont nécessaires.

### Copier un fichier local vers le serveur distant

Copie un fichier du disque courant vers le serveur distant.

-(NSString*)copierFichier:(NSString*)cheminlocal vers:(NSString*)chemindistant

### Supprimer un fichier distant

Efface un fichier sur le serveur distant.

-(BOOL) effacerfichier:(NSString*)chemin

### Remplacer un fichier distant par un autre

Remplace un fichier distant par un fichier local.

-(BOOL)remplacerFichierActuel:(NSString*)cheminFichierActuel parFichier:(NSString*)cheminFichierRemplacant garderMemeNom:(BOOL)garderMemeNom

### Créer un répertoire sur le serveur distant

Créé un nouveau répertoire sur le serveur distant.

-(NSString*)creerRepertoire:(NSString*)chemin

### Supprimer un répertoire sur le serveur distant

Supprime un répertoire existant sur le serveur distant.

-(NSString*)supprimerRepertoire:(NSString*)chemin

### Renommer un répertoire sur le serveur distant

Renomme un répertoire existant sur le serveur distant.

-(NSString*)renommerRepertoire:(NSString*)chemin en:(NSString*)nouveauNom

### Savoir si le chemin distant correspond à un fichier ou un répertoire

-(int) estUnRepertoire:(NSString*)chemin

### Savoir si le fichier ou répertoire distant existe

-(BOOL)existe:(NSString*)nomFichierOuRepertoire

### Récupérer un fichier distant en local

Télécharge un fichier du serveur distant vers le disque local.

-(NSString*)copierFichierDeBDD:(NSString*)chemindistant vers:(NSString*)cheminlocal

### Générer un nom de fichier unique

Génere un nom de fichier de type GUID.

- (NSString*)genererNomUnique:(NSString*)nom

## Exemples de manipulation de la base de données

### Création d'un objet

TODO

### Insertion d'un objet

TODO

### Modification d'un objet

TODO

### Suppression d'un objet

TODO
